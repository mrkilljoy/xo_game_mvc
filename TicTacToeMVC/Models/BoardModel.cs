﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToeMVC.Models
{
    public abstract class Board
    {
        /// <summary>
        /// Array that contains all of the spaces on the board.
        /// </summary>
        public Player[,] squares;

        /// <summary>
        /// Gets or sets a space on the board / 'player move'
        /// </summary>
        public abstract Player this[int x, int y] { get; set; }

        /// <summary>
        /// Determines if all spaces on the board are full.
        /// </summary>
        public abstract bool IsFull { get; }

        /// <summary>
        /// Gets the maximum size of the board.
        /// </summary>
        public abstract int Size { get; }

        /// <summary>
        /// List of the open spaces available on the current board.
        /// </summary>
        public abstract List<Space> OpenSquares { get; }

        /// <summary>
        /// Determines if there is a winner on the current board.
        /// </summary>
        public abstract Player Winner { get; }

        /// <summary>
        /// Makes a deap copy of the current board
        /// </summary>
        public abstract Board Clone();
    }

    public class ActualBoard : Board
    {
        public ActualBoard()
        {
            squares = new Player[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        }

        public override int Size { get { return 9; } }

        public override Player this[int x, int y]
        {
            get
            {
                return squares[x, y];
            }

            set
            {
                squares[x, y] = value;
            }
        }

        public override bool IsFull
        {
            get
            {
                foreach (Player i in squares)
                    if (i == Player.Open) { return false; }
                return true;
            }
        }

        public override List<Space> OpenSquares
        {
            get
            {
                List<Space> openSquares = new List<Space>();

                for (int x = 0; x <= 2; x++)
                    for (int y = 0; y <= 2; y++)
                        if (squares[x, y] == Player.Open)
                            openSquares.Add(new Space(x, y));

                return openSquares;
            }
        }

        public override Player Winner
        {
            get
            {
                int count = 0;

                //columns
                for (int x = 0; x < 3; x++)
                {
                    count = 0;

                    for (int y = 0; y < 3; y++)
                        count += (int)squares[x, y];

                    if (count == 3)
                        return Player.X;
                    if (count == -3)
                        return Player.O;
                }

                //rows
                for (int x = 0; x < 3; x++)
                {
                    count = 0;

                    for (int y = 0; y < 3; y++)
                        count += (int)squares[y, x];

                    if (count == 3)
                        return Player.X;
                    if (count == -3)
                        return Player.O;
                }

                //diagnols right to left
                count = 0;
                count += (int)squares[0, 0];
                count += (int)squares[1, 1];
                count += (int)squares[2, 2];
                if (count == 3)
                    return Player.X;
                if (count == -3)
                    return Player.O;

                //diagnols left to right
                count = 0;
                count += (int)squares[0, 2];
                count += (int)squares[1, 1];
                count += (int)squares[2, 0];
                if (count == 3)
                    return Player.X;
                if (count == -3)
                    return Player.O;

                return Player.Open;
            }
        }

        public override Board Clone()
        {
            Board b = new ActualBoard();
            b.squares = (Player[,])this.squares.Clone();
            return b;
        }

        public static Space PositionToSpace(int pos)
        {
            var point = new Space();
            switch (pos)
            {
                case 1:
                    {
                        point.X = 0;
                        point.Y = 0;
                        break;
                    }
                case 2:
                    {
                        point.X = 0;
                        point.Y = 1;
                        break;
                    }
                case 3:
                    {
                        point.X = 0;
                        point.Y = 2;
                        break;
                    }
                case 4:
                    {
                        point.X = 1;
                        point.Y = 0;
                        break;
                    }
                case 5:
                    {
                        point.X = 1;
                        point.Y = 1;
                        break;
                    }
                case 6:
                    {
                        point.X = 1;
                        point.Y = 2;
                        break;
                    }
                case 7:
                    {
                        point.X = 2;
                        point.Y = 0;
                        break;
                    }
                case 8:
                    {
                        point.X = 2;
                        point.Y = 1;
                        break;
                    }
                case 9:
                    {
                        point.X = 2;
                        point.Y = 2;
                        break;
                    }
            }
            return point;
        }

        public GameReply CheckForWinners()
        {
            Player? p = this.Winner;

            if (p == Player.X)
                return GameReply.CpuWins;
            else if (p == Player.O)
                return GameReply.PlayerWins;
            else if (this.IsFull)
                return GameReply.Draw;
            return GameReply.Continue;
        }

        public static GameReply MakeCpuTurn(ActualBoard board, int point)
        {
            Space player_spot = ActualBoard.PositionToSpace(point);
            if (board[player_spot.X, player_spot.Y] != Player.Open)
                return GameReply.Continue;
            else
                board[player_spot.X, player_spot.Y] = Player.O;

            if (board.CheckForWinners() != GameReply.Continue)
                return board.CheckForWinners();

            Space? cpu_spot = CPU.GetBestMove(board, Player.X);
            if (cpu_spot == null)
                return GameReply.Error;
            else
            {
                board[cpu_spot.Value.X, cpu_spot.Value.Y] = Player.X;
                if (board.CheckForWinners() != GameReply.Continue)
                    return board.CheckForWinners();
                else
                    return GameReply.Continue;
            }
        }
    }

    public struct Space
    {
        public int X;
        public int Y;
        public double Rank;

        public Space(int x, int y)
        {
            this.X = x;
            this.Y = y;
            Rank = 0;
        }
    }

    public enum GameReply
    {
        PlayerWins,
        CpuWins,
        Draw,
        Continue,
        Error
    }

}