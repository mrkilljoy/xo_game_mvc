﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TicTacToeMVC.Models
{
    public class ScoreContext : DbContext
    {
        public ScoreContext() : base("ScoreContext") { }

        public DbSet<ScoreRecord> Records { get; set; }
    }
}