﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToeMVC.Models
{
    public enum Player
    {
        X = 1,
        O = -1,
        Open = 0,
    }
}