﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToeMVC.Models
{
    // A single score record
    public class ScoreRecord
    {
        public int Id { get; set; }

        public string Player { get; set; }

        // game result
        public string Result { get; set; }

        // player move list (includes repeats)
        public string MoveList { get; set; }
    }
}