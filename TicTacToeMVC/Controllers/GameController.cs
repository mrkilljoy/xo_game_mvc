﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicTacToeMVC.Models;

namespace TicTacToeMVC.Controllers
{
    public class GameController : Controller
    {
        ScoreContext db = new ScoreContext();

        [HttpGet]
        public ActionResult Game()
        {
            ScoreContext sc = new ScoreContext();
            TempData["Movelist"] = "";

            for (int i = 0; i < 9; i++)
            {
                var btn_name = string.Format("btn{0}", i + 1);
                TempData[btn_name] = "";
            }
            return View(new ActualBoard());
        }

        [HttpPost]
        public ActionResult Game(ActualBoard bname)
        {
            int num = SetPlayerMark();
            ActualBoard brd = TempData["ModelData"] as ActualBoard;
            string result = CheckSpacesContent(num, brd);
            
            if (result == null)
                return View(brd);
            else
                return Result(ParseResultText(result));
        }

        [HttpGet]
        public ActionResult Result(string result)
        {
            return View("SubmitResult", (object)result);
        }

        [HttpPost]
        public ActionResult Result()
        {
            ScoreRecord rec = new ScoreRecord();
            if (Request.Params["result"] == null)
                rec.Player = "noname";
            else
                rec.Player = (string)Request.Params["result"];
            rec.Result = (string)TempData["Result"];
            rec.MoveList = (string)TempData["Movelist"];

            db.Records.Add(rec);
            db.SaveChanges();

            return View("ResultSubmitted");
        }

        // Set button value as player mark
        public int SetPlayerMark()
        {
            int btn_number = 0;
            for (int i = 0; i < 9; i++)
            {
                var btn_name = string.Format("btn{0}", i + 1);
                if (Request.Params[btn_name] != null)
                {
                    TempData[btn_name] = "O";
                    btn_number = i + 1;
                }
            }

            return btn_number;
        }

        // Set CPU mark on the field
        public string CheckSpacesContent(int num, ActualBoard brd)
        {
            if (num == 0)
                return null;
            else
            {
                var result = ActualBoard.MakeCpuTurn(brd, num);

                // saving player's move
                if (TempData["Movelist"] != null)
                    TempData["Movelist"] += string.Format("{0};", num);

                if (result == GameReply.Continue)
                {
                    for (int i = 0; i < brd.Size; i++)
                    {
                        Space cell = ActualBoard.PositionToSpace(i + 1);
                        if (brd[cell.X, cell.Y] == Player.X)
                        {
                            string name = string.Format("btn{0}", i + 1);
                            TempData[name] = "X";
                        }
                    }
                    return null;
                }
                else
                {
                    //saving game result
                    TempData["Result"] = result.ToString();
                    return result.ToString();
                }
            }
        }

        public string ParseResultText(string txt)
        {
            if (txt == "CpuWins")
                return "You lose";
            else if (txt == "PlayerWins")
                return "You win";
            else if (txt == "Draw")
                return "Draw";
            else
                return "GAME_ERROR";
        }
    }
}